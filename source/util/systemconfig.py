from ConfigParser import *

class SystemConfig(object):
	"""docstring for SystemConfig"""
	def __init__(self, file, myselfName):
		super(SystemConfig, self).__init__()
		self.file = file
		self.myselfName = myselfName
		self.servers = []
		self.services = []
		self.myself = None
		self.parse()

	def parse(self):
		self.servers = []
		self.myself = None
		cp = ConfigParser()
		cp.read(self.file)
		self.system_config = cp
		self.options = itemsToOptions(cp.items('system'))
		if self.myselfName == None:
			return
	def get_database_config(self,db_name):
		user = self.system_config.get(db_name, 'user')
		password = self.system_config.get(db_name, 'password')
		database = self.system_config.get(db_name, 'database')

		if self.system_config.has_option(db_name, 'db_host'):
			db_host = self.system_config.get(db_name, 'db_host')

		if self.system_config.has_option(db_name, 'db_port'):
			db_port = self.system_config.get(db_name, 'db_port')

		if self.system_config.has_option(db_name, 'pool_size'):
			pool_size = self.system_config.get(db_name, 'pool_size')
			
		return user, password, database, db_host, db_port, pool_size

def itemsToOptions(items):
	options = {}
	for item in items:
		options[item[0]] = item[1]
	return options