
from proto import *
import proto

class MessageDef(object):
    def __init__(self,service,msg_cls):
        self.service = service
        self.msg_id = msg_cls.ID
        self.msg_cls = msg_cls

class MessageMapping(object):
    _mapping = {}

    @staticmethod
    def init():
        if len(MessageMapping._mapping) != 0:
            return

        pb2s = []

        tmp = dir(proto)
        for p in tmp:

            if p.endswith("pb2"):
                pb2s.append(getattr(proto,p))

        for pb2 in pb2s:
            tmp = dir(pb2)
            for c in tmp :
                cls = getattr(pb2,c)

                if c.endswith("Req") or c.endswith("Resp") or c.endswith("Event"):
                    if hasattr(cls,"DEF") and hasattr(cls,"ID"):
                        MessageMapping._mapping[cls.ID] = MessageDef(None,cls)